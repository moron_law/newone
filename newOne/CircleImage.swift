//
//  CircleImage.swift
//  newOne
//
//  Created by Max Ro on 05.05.2020.
//  Copyright © 2020 Max Ro. All rights reserved.
//

import SwiftUI

struct CircleImage: View {
    var body: some View {
        Image("turtlerock")
         .clipShape(Circle())
        .overlay(
            Circle().stroke(Color.gray, lineWidth: 1))
        .shadow(radius: 10)
    }
}

struct CircleImage_Previews: PreviewProvider {
    static var previews: some View {
        CircleImage()
    }
}
