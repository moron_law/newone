//
//  ContentView.swift
//  newOne
//
//  Created by Max Ro on 06.04.2020.
//  Copyright © 2020 Max Ro. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        
        VStack() {
            MapView()
                .edgesIgnoringSafeArea(.top)
                .frame(height: 500.0)
            

            
            CircleImage()
            .offset(y: -130)
            .padding(.bottom, -130)
            
            VStack(alignment: .leading) {
                Text("Turtlerock")
                    .font(.title)
                    .foregroundColor(Color(red: 0.8, green: 0.0, blue: 0.6, opacity: 1.0))
                HStack {
                    Text("Joshua Tree National Park")
                        .font(.subheadline)
                    Spacer()
                    Text("California")
                    .font(.subheadline)
                    
                }
            }
            .padding()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
